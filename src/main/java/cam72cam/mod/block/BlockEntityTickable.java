package cam72cam.mod.block;

public abstract class BlockEntityTickable extends BlockEntity {
    public abstract void update();
}
